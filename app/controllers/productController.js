const productModel = require("../model/productModel");
//function tạo product mới
const createProduct = (request, response) =>{
    const body = request.body;

    if(!body.Id){
        return response.status(400).json({
            status:"Bad Request",
            message: "Id Product không hợp lệ"
        })
    }
    if(!body.Name){
        return response.status(400).json({
            status:"Bad Request",
            message: "Name Product không hợp lệ"
        })
    }
    if(!body.Type){
        return response.status(400).json({
            status:"Bad Request",
            message: "Type Product không hợp lệ"
        })
    }
    if(!body.ImageUrl){
        return response.status(400).json({
            status:"Bad Request",
            message: "ImageUrl Product không hợp lệ"
        })
    }
    if(!body.BuyPrice){
        return response.status(400).json({
            status:"Bad Request",
            message: "BuyPrice Product không hợp lệ"
        })
    }
    if(!body.PromotionPrice){
        return response.status(400).json({
            status:"Bad Request",
            message: "PromotionPrice Product không hợp lệ"
        })
    }

    const newProduct = {
        Id: body.Id,
        Name: body.Name,
        Type: body.Type,
        ImageUrl: body.ImageUrl,
        BuyPrice: body.BuyPrice,
        PromotionPrice: body.PromotionPrice,
        Description: body.Description
    }
    productModel.create(newProduct,(error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            message:"Create new product successfully",
            Product: data
        })
    })
}
//tạo function get product
const getAllProduct = (request, response) => {
    productModel.find((error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all Products successfully",
            data: data
        })
    })
}
const getProductByType = (request, response)=> {
    const DescriptionProduct =  request.query.DescriptionProduct;
    if(DescriptionProduct){
    productModel.find({Description: DescriptionProduct}, (errorFindProduct, DescriptionProductExist) => {
        if(errorFindProduct){
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindProduct.message
            })
        } else{
            if(!DescriptionProduct){ 
                return response.status(400).json({
                    status:" Type product is not valid",
                    data: []
                })
            } else {
                productModel.find({Description: DescriptionProduct} ,(error, data) =>{
                    if(error){
                        return response.status(500).json({
                            status:" Internal server error",
                            message: error.message
                        })
                    }
                    return response.status(200).json({
                        status:`Get product by ${DescriptionProduct} successfully`,
                        Product: data
                    })
                })
            }
        }
    })
    }
}
//530.30 call api limit product shop 24h
const getProductLimit = (request, response) =>{
    const limitNumber = request.query.limitNumber;
    if(limitNumber){
        productModel.find()
            .limit(limitNumber)
            .exec((err,data) =>{
                if(err){
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: err.message
                    })
                }
                else{
                    return response.status(200).json({
                        status:"Get limited product successfully",
                        data: data
                    })
                }
            })}
        else{
            productModel.find((error, data) =>{
                if(error){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: error.message
                    })
                }
                return response.status(200).json({
                    status:"Get all Products successfully",
                    data: data
                })
            })
        }
    
}
module.exports = {
    createProduct,
    getAllProduct,
    getProductByType,
    getProductLimit
};
