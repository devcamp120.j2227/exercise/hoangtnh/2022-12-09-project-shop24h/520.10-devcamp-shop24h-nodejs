const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new  Schema ({
    Id: {
        type: Number,
        require: true
    },
    Name: {
        type: String,
        require: true
    },
    Type: {
        type: String,
        require: true
    }, 
    ImageUrl: {
        type: String,
        require: true
    },
    BuyPrice: {
        type: Number,
        require: true
    }, 
    PromotionPrice: {
        type: Number,
        require: true
    },
    Description: {
        type: String,
        require: true
    }

});

module.exports = mongoose.model("Products", productSchema);