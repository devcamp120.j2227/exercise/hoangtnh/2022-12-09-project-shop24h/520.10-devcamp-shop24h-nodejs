const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

router.post("/products", productController.createProduct);
router.get("/products", productController.getAllProduct);
router.get("/products/description", productController.getProductByType);
router.get("/limit-products",productController.getProductLimit);
module.exports = router;